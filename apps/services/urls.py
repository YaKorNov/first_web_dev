# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from views import test_view

urlpatterns = [
    url(r'^index/$', test_view, name='index')
]
