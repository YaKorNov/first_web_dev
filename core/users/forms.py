# coding=utf-8
from models import User
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.contrib import auth
from django import forms
from django.utils.translation import ugettext_lazy as _



class AdminUserAddForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('email', 'phone','first_name','last_name')


class AdminUserChangeForm(UserChangeForm):

    class Meta(UserChangeForm.Meta):
        model = User
        fields = '__all__'


class RegistrationForm(forms.ModelForm):

    required_css_class = 'required'

    class Meta:
        model = User
        fields = ("email",)

    def clean(self):
        cleaned_data = super(forms.ModelForm, self).clean()
        """
        если email пройдет встроенную проверку
        """
        try:
            existing = auth.get_user_model().objects.filter(email__iexact=cleaned_data['email'])
            if existing.exists():
                 raise forms.ValidationError(_("A user with that e-mail already exists."))
            else:
                return cleaned_data
        except:
            pass



# class RegistrationForm(forms.Form):
#
#     required_css_class = 'required'
#
#     email = forms.EmailField(label=_("E-mail"))
#
#     def clean(self):
#
#         existing = auth.get_user_model().objects.filter(email__iexact=self.cleaned_data['email'])
#         if existing.exists():
#             raise forms.ValidationError(_("A user with that e-mail already exists."))
#         else:
#             return self.cleaned_data
