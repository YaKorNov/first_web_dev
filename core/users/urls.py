# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from views import LogOutView
from django.views.generic.base import TemplateView
from django.conf.urls import patterns, include, url
from registration.backends.default.views import ActivationView
from views import RegistrationView
from django.contrib.auth.decorators import login_required
from utils import GuestRequired
from django.contrib.auth.views import login


urlpatterns = [
    url(r'^login/$', GuestRequired(login), name='login'),
    url(r'^logout/$', LogOutView.as_view(), name='logout'),
    url(r'^user/password/reset/$',
        'django.contrib.auth.views.password_reset',
        {
            'post_reset_redirect': '/user/password/reset/done/',
        },
        name="password_reset"),
    url(r'^user/password/reset/done/$', 'django.contrib.auth.views.password_reset_done'),
    url(r'^user/password/reset/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', 'django.contrib.auth.views.password_reset_confirm',
        {
            'post_reset_redirect': '/user/password/done/'
        },
        name='password_reset_confirm'),
    url(r'^user/password/done/$', 'django.contrib.auth.views.password_reset_complete'),
    url(r'^activate/complete/$',
       TemplateView.as_view(template_name='registration/activation_complete.html'),
       name='registration_activation_complete'),
       # Activation keys get matched by \w+ instead of the more specific
       # [a-fA-F0-9]{40} because a bad activation key should still get to the view;
       # that way it can return a sensible "invalid key" message instead of a
       # confusing 404.
    url(r'^activate/(?P<activation_key>\w+)/$',
       ActivationView.as_view(),
       name='registration_activate'),
    url(r'^register/$',
       GuestRequired(RegistrationView.as_view()),
       name='registration_register'),
    url(r'^register/complete/$',
       TemplateView.as_view(template_name='registration/registration_complete.html'),
       name='registration_complete'),
    url(r'^register/closed/$',
       TemplateView.as_view(template_name='registration/registration_closed.html'),
       name='registration_disallowed'),
    # url('', include('social.apps.django_app.urls', namespace='social'))
    url(r'', include('social_auth.urls')),
    url(r'^user_profile/$',
       login_required(TemplateView.as_view(template_name='registration/user_profile.html')),
       name='user_profile'),

]
