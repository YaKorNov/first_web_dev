# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import ugettext_lazy as _

from forms import AdminUserChangeForm, AdminUserAddForm
from models import User


class UserAdmin(BaseUserAdmin):
    form = AdminUserChangeForm
    add_form = AdminUserAddForm
    add_fieldsets = (
        (None, {
            'fields': ('email', 'phone','first_name','last_name',
                       'password1',
                       'password2', ),
        }),
    )
    ordering = ('email',)
    list_display = ('email', 'first_name', 'last_name', 'phone',
                    'is_active', 'is_staff')
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups')
    list_display_links = ('email', )
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': (
            'username',
            'first_name',
            'last_name',
            'phone',
        )}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )



admin.site.register(User, UserAdmin)

