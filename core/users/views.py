# -*- coding: utf-8 -*-
from django.views.generic.base import View
from django.contrib import auth, messages
from django.shortcuts import redirect
from django.core.urlresolvers import reverse_lazy
from registration.backends.default.views import RegistrationView as BaseRegistrationView
from registration.users import UserModel
from forms import RegistrationForm

from django.shortcuts import redirect

from registration import signals

from django.contrib.sites.models import RequestSite,Site

from registration.models import RegistrationProfile
from django.views.generic.base import TemplateView
from django.conf import settings



class LogOutView(View):
    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            auth.logout(request)
        return redirect(request.META.get('HTTP_REFERER') or reverse_lazy("services:index"))


class RegistrationView(BaseRegistrationView):

    # disallowed_url = 'registration_disallowed'
    form_class = RegistrationForm
    template_name = 'registration/registration_form.html'

    def register(self, request, form):

        if Site._meta.installed:
            site = Site.objects.get_current()
        else:
            site = RequestSite(request)

        # if hasattr(form, 'save'):
        #     new_user_instance = form.save()
        # else:
        new_user_instance = UserModel().objects.create_user(**form.cleaned_data)

        new_user = RegistrationProfile.objects.create_inactive_user(
            new_user=new_user_instance,
            site=site,
            send_email=self.SEND_ACTIVATION_EMAIL,
            request=request,
        )
        signals.user_registered.send(sender=self.__class__,
                                     user=new_user,
                                     request=request)
        return new_user


    # def register(self, request, **cleaned_data):
    #     """
    #     Given  email address  register a new
    #     user account, which will initially be inactive.
    #
    #     Along with the new ``User`` object, a new
    #     ``registration.models.RegistrationProfile`` will be created,
    #     """
    #     email = cleaned_data['email']
    #     if Site._meta.installed:
    #         site = Site.objects.get_current()
    #     else:
    #         site = RequestSite(request)
    #     new_user = RegistrationProfile.objects.create_inactive_user(email,
    #                                                                 site)
    #     signals.user_registered.send(sender=self.__class__,
    #                                  user=new_user,
    #                                  request=request)
    #     return new_user

# class RegistartionCompleteView(TemplateView):
#     template_name='registration/registration_complete.html'
#
#     def get_context_data(self, **kwargs):
#         context = super(RegistartionCompleteView, self).get_context_data(**kwargs)
#         context['password'] = settings.USER_REGISTRATION_PASSWORD
#         return context
