# coding=utf-8

from django.utils import timezone
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager

import datetime

from django.conf import settings
from django.db import models
from django.db import transaction
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _
from registration.models import RegistrationManager as BaseRegistrationManager
from registration.models import RegistrationProfile as BaseRegistrationProfile
from registration.users import UserModel
from django.contrib.auth import get_user_model
from django.core.mail import EmailMultiAlternatives
from django.template import RequestContext, TemplateDoesNotExist


try:
    from django.utils.timezone import now as datetime_now
except ImportError:
    datetime_now = datetime.datetime.now



class UserManager(BaseUserManager):
    def _create_user(self, email, phone,  password,
                     is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        now = timezone.now()
        if not email:
            raise ValueError('The given email must be set')

        user = self.model(email=email, phone=phone,
                          is_staff=is_staff, is_active=True,
                          is_superuser=is_superuser, last_login=now,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, phone=None, **extra_fields):
        # return self._create_user(email, phone, self.make_random_password(), False, False,
        #                          **extra_fields)
        return self._create_user(email, phone, settings.USER_REGISTRATION_PASSWORD, False, False,
                                 **extra_fields)

    def create_superuser(self, email, password, phone=None, **extra_fields):
        return self._create_user(email, phone,  password, True, True,
                                 **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(u'Имя пользователя для соц.сетей', max_length=255, blank=True)
    email = models.EmailField(u'Email', unique=True)
    phone = models.CharField(u'Мобильный телефон', max_length=30, unique=True, null=True, blank=True)
    first_name = models.CharField(u'Имя', max_length=255, blank=True)
    last_name = models.CharField(u'Фамилия', max_length=255, blank=True)
    middle_name = models.CharField(u'Отчество', max_length=255, blank=True)
    is_staff = models.BooleanField(u'Является персоналом', default=False)
    is_active = models.BooleanField(u'Активен', default=True)
    date_joined = models.DateTimeField(u'Дата регистрации', default=timezone.now)

    objects = UserManager()

    USERNAME_FIELD = 'email'

    class Meta:
            verbose_name = _(u'Пользователь')
            verbose_name_plural = _(u'Пользователи')

    def get_full_name(self):
        return (u'%s %s %s' % (self.last_name, self.first_name, self.middle_name)).strip(u' ')

    def get_short_name(self):
        return self.first_name



# class RegistrationManager(BaseRegistrationManager):

    # def create_inactive_user(self,  email,
    #                          site, send_email=True):
    #
    #     new_user = get_user_model().objects.create_user( email)
    #     new_user.is_active = False
    #     new_user.save()
    #
    #     registration_profile = self.create_profile(new_user)
    #
    #     if send_email:
    #         registration_profile.send_activation_email(site)
    #
    #     return new_user
    # create_inactive_user = transaction.commit_on_success(create_inactive_user)



# class RegistrationProfile(BaseRegistrationProfile):
#
#
#     objects = RegistrationManager()
#
#     class Meta:
#         proxy = True


