from functools import wraps
from django.shortcuts import redirect
from django.conf import settings
from django.http import HttpResponseBadRequest,HttpResponseNotFound

def GuestRequired(view_func):
    @wraps(view_func)
    def _wrapped_view(request, *args, **kwargs):
        if request.user.is_authenticated():
            return redirect('user_profile')
        return view_func(request, *args, **kwargs)
    return _wrapped_view


def user_pass_context(request):

    return {'password': settings.USER_REGISTRATION_PASSWORD}