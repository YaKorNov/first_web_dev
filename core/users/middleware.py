# -*- coding: utf-8 -*-
from social_auth.middleware import SocialAuthExceptionMiddleware
from social_auth.exceptions import AuthFailed,AuthAlreadyAssociated
#AuthForbidden
from django.contrib import messages

class CustomSocialAuthExceptionMiddleware( SocialAuthExceptionMiddleware):

    def get_message(self, request, exception):
        msg = None
        if isinstance(exception, AuthFailed):
            msg =   u"Неудачная попытка аутентификации в "
        elif isinstance(exception, AuthAlreadyAssociated):
            msg =   u"Этот аккаунт в соцсети уже занят другим пользователем сайта"
        # elif isinstance(exception, AuthForbidden):
        #     msg =   u"Данные, предоставленные вашим аккаунтом в соцсети не позволяют войти на сайт"
        else:
            msg =   u"Неизвестная ошибка аутентификации."
        messages.add_message(request, messages.ERROR, msg)