from django import template
from django.conf import settings

register = template.Library()


@register.filter
def backend_class(backend):
    return backend.replace('-', ' ')


@register.inclusion_tag('registration/social_auth_widget.html', takes_context=True)
def social_auth_widget(context):

    return {
        'social_auth_icons_positions': settings.SOCIAL_AUTH_PROVIDERS_ICONS,
        'social_auth':context['social_auth']
    }

@register.assignment_tag
def get_item(dictionary, key):
    return dictionary.get(key)

