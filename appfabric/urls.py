from django.conf.urls import patterns, include, url
from django.contrib import admin

from apps.services.views import test_view

urlpatterns = patterns('',
    url(r'', include('apps.services.urls','services')),
    url(r'', include('core.users.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
