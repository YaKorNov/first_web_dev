# -*- coding: utf-8 -*-
import os
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse_lazy

BASE_DIR = os.path.dirname(os.path.join(os.path.dirname(os.path.abspath(__file__)), '../'))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = ')l*ou+34)ixg&s$oztz^9dqq%q)%t)4*ayv*9$4u-kd^956!x2'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = [
    'localhost',
    'localhost:8000',
    '.appfabric.com'
]

# Application definition
INSTALLED_APPS = (
    'apps.services',
    'core.users',
    'suit',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'registration',
    # 'social.apps.django_app.default',
    'social_auth',
    'widget_tweaks',
    'annoying',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'core.users.middleware.CustomSocialAuthExceptionMiddleware',
)

ROOT_URLCONF = 'appfabric.urls'
WSGI_APPLICATION = 'appfabric.wsgi.application'

AUTH_USER_MODEL = 'users.User'

SOCIAL_AUTH_USER_MODEL = 'users.User'

# SOCIAL_AUTH_USER_MODEL = 'users.User'
#
# AUTHENTICATION_BACKENDS = (
#     'social.backends.vk.VKOAuth2',
#     'social.backends.google.GoogleOAuth2',
#     'social.backends.bitbucket.BitbucketOAuth2',
#     'django.contrib.auth.backends.ModelBackend',
# )

AUTHENTICATION_BACKENDS = (
    'social_auth.backends.contrib.vk.VKOAuth2Backend',
    'social_auth.backends.google.GoogleOAuth2Backend',
    'social_auth.backends.contrib.mailru.MailruBackend',
    'social_auth.backends.contrib.yandex.YandexOAuth2Backend',
    'social_auth.backends.twitter.TwitterBackend',
    'django.contrib.auth.backends.ModelBackend',
)

SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/user_profile'
# SOCIAL_AUTH_LOGIN_ERROR_URL = '/login-error/'
# SOCIAL_AUTH_LOGIN_URL = '/login-url/'

LOGIN_URL = 'login'
LOGIN_REDIRECT_URL = 'services:index'
# LOGIN_REDIRECT_URL = 'services:index'
LOGIN_ERROR_URL = 'login'

SOCIAL_AUTH_PROTECTED_USER_FIELDS = ['email',]

SOCIAL_AUTH_PROVIDERS_ICONS = {
    p[0]:{ 'name': p[1], 'width': p[2][0], 'height': p[2][1], }
    for p in
        [
            ('vk-oauth', u'Login via VK', (0, 0)),
            ('google-oauth2', u'Login via Google', (0, -175)),
            ('yandex-oauth2', u'Login via Yandex', (0, -210)),
            ('twitter', u'Login via Twitter', (0, -140)),
            ('mailru-oauth2', u'Login via Mail.ru', (0, -70)),
        ]
    }

# SOCIAL_AUTH_PROTECTED_USER_FIELDS = ['email',]
# SOCIAL_AUTH_ADMIN_USER_SEARCH_FIELDS = ['username', 'email']
#
# SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '476144904789-u05pi84it7tpto5npa9205t89qn19c8e.apps.googleusercontent.com'
# SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = 'GNROd36fcQBqv5qSRvFn_S_V'
#
# SOCIAL_AUTH_VK_OAUTH2_KEY = '5012948'
# SOCIAL_AUTH_VK_OAUTH2_SECRET = 'crBqK56B7y67YFXn3DQo'
#
# SOCIAL_AUTH_BITBUCKET_OAUTH2_KEY = 'GrrnMUvESLJNAEQvhy'
# SOCIAL_AUTH_BITBUCKET_OAUTH2_SECRET = 'XVHGB2EWfZWr3ZzRvJebc89kQZWStnw6'

VK_APP_ID = '5012948'
VK_API_SECRET = 'crBqK56B7y67YFXn3DQo'

TWITTER_CONSUMER_KEY = 'AgSYDYPzN5O8OyFbeRdwjqpFF'
TWITTER_CONSUMER_SECRET = 'RYcKtvOws3qfgN0UWVUCzSIkVB9GgzNbsUHRido6LTinlWREPK'

MAILRU_OAUTH2_CLIENT_KEY = '736153'
MAILRU_OAUTH2_CLIENT_SECRET = 'b1ce54a9f5699c6172b4166a5253f23a'

GOOGLE_OAUTH2_CLIENT_ID = '476144904789-u05pi84it7tpto5npa9205t89qn19c8e.apps.googleusercontent.com'
GOOGLE_OAUTH2_CLIENT_SECRET = 'GNROd36fcQBqv5qSRvFn_S_V'

YANDEX_APP_ID = '21f9b0f82c80465eadaa4782b852af53'
YANDEX_API_SECRET = '6ea6d10f8e4f4a46a2003035a16b64c4'
YANDEX_OAUTH2_API_URL = 'https://api-yaru.yandex.ru/me/'

SESSION_SERIALIZER='django.contrib.sessions.serializers.PickleSerializer'

# Database
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'appfabric_base',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'localhost',
    }
}

# Internationalization
LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'UTC'

USE_I18N = True
USE_L10N = True
USE_TZ = True

ACCOUNT_ACTIVATION_DAYS = 7
REGISTRATION_EMAIL_HTML = True

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'www/static_collector')

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'www/media')

STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(BASE_DIR, 'static'),
)



TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.core.context_processors.request',
                # 'social.apps.django_app.context_processors.backends',
                # 'social.apps.django_app.context_processors.login_redirect',
                'social_auth.context_processors.social_auth_backends',
                'core.users.utils.user_pass_context',
            ],
        },
    },
]



try:
    from settings_local import *
except ImportError:
    pass

if DEBUG:
    DEBUG_TOOLBAR_PANELS = [
        'debug_toolbar.panels.versions.VersionsPanel',
        'debug_toolbar.panels.timer.TimerPanel',
        'debug_toolbar.panels.settings.SettingsPanel',
        'debug_toolbar.panels.headers.HeadersPanel',
        'debug_toolbar.panels.request.RequestPanel',
        'debug_toolbar.panels.sql.SQLPanel',
        'debug_toolbar.panels.staticfiles.StaticFilesPanel',
        'debug_toolbar.panels.templates.TemplatesPanel',
        'debug_toolbar.panels.cache.CachePanel',
        'debug_toolbar.panels.signals.SignalsPanel',
        'debug_toolbar.panels.logging.LoggingPanel',
        'debug_toolbar.panels.redirects.RedirectsPanel',
    ]

    # DEBUG_TOOLBAR_CONFIG = {
    #     'INTERCEPT_REDIRECTS': False,
    #     'HIDE_DJANGO_SQL': False,
    #     'JQUERY_URL':'//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js'
    # }
    INTERNAL_IPS = ('127.0.0.1', '10.0.2.2')

    MIDDLEWARE_CLASSES += ('debug_toolbar.middleware.DebugToolbarMiddleware',)
    INSTALLED_APPS += ('debug_toolbar',)

# Django suit
SUIT_CONFIG = {
    'ADMIN_NAME': 'Администрирование',
    'SEARCH_URL': '',
    'MENU': (
        {
            'label': _(u'Пользователи'),
            'icon': 'icon-user',
            'models': (
                'core.users.user',
                'auth.group'
            )
        },

    )
}


USER_REGISTRATION_PASSWORD = 'my_pass'

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_USE_SSL = True
EMAIL_PORT = 465
# EMAIL_USE_TLS = True
# EMAIL_PORT = 587
EMAIL_HOST_PASSWORD = ''
EMAIL_HOST_USER = 'yarkornienko@gmail.com'